# Wefox Cool Cities App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

You can see the project at [https://cool-cities.netlify.com](https://cool-cities.netlify.com).

## Description

This is a simple SPA App built in React. React makes it easier to work with a "functional components mindset", keeping the presentational parts isolated from the logic and the state connectors (also known as containers). I like that React is less “frameworky” than Vue or Angular, so at the end I feel like I’m just writing plain JS with some helpers for rendering and dealing with states.

I tend to organise my code with a “domain driven mindset”, trying to follow the business logic and keeping the different responsabilities of my application separated from the others, which makes it easier to tests and scale. Domains are organized by subdirectories inside the `/components` folder, which contains all the files involving that domain ("container", components, state, tests, styles, etc.).

I’ve been using Redux as the main state manager in almost every large project I’ve worked on lately. For this project I switched to the useState hook (using Redux would be overengineering!). I centralised the business logic in only a few components, so I could keep most of them functional (which are much more easier to test).

For unit testing I used Jest along with React Testing Library. It was my first time using the last one and I found it very interesting because it centers the testing on the user experience and the outputs, avoiding implementation details that surely will change in the future. So goodbye Enzyme!

For styling I used styled-components, which makes it really easy to keep the scope of the styles inside the component. I also created some base global styles (let's not lose the "cascading" feature of CSS completely!).

## What would I add if I had more time

- Some unit tests focused on diferent user interactions.
- E2E testing some usecases, using TestCafe for example.
- API and validation error handling.
- Better responsiveness, specially for mobile.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
