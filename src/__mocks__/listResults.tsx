export default [
  {
    id: 5,
    title: "Tokio",
    content:
      "El Área del Gran Tokio se denomina a la amplia área metropolitana de Tokio, Japón. Es una de las áreas metropolitanas con mayor población en el mundo. En japonés, Área del Gran Tokio se puede referir tanto a Área de Tokio (東京圏 Tōkyō-ken?), como a Capital (首都圏 Shuto-ken?) o Sur de Kanto (南関東 Minami-Kantō?). Esta área es la primera en el mundo en el uso de tránsito rápido, con 22 millones de pasajeros que utilizan diariamente las 136 líneas de tren.",
    lat: "35.6894989",
    long: "139.6917100",
    image_url:
      "https://heymondo.es/blog/wp-content/uploads/2018/08/tokio-en-3-d%C3%ADas.jpg",
    created_at: "2020-04-15T02:03:22.618Z",
    updated_at: "2020-04-15T02:03:54.209Z",
  },
  {
    id: 3,
    title: "Berlin City",
    content:
      "Berlin hey hey is the capital and the largest city of Germany as well as one of its 16 constituent states. With a population of approximately 3.7 million, Berlin is the second... Berlin hey hey is the capital and the largest city of Germany as well as one of its 16 constituent states. With a population of approximately 3.7 million, Berlin is the second...",
    lat: "52.5065133",
    long: "13.1445548",
    image_url:
      "https://www.visitberlin.de/system/files/styles/visitberlin_teaser_single_visitberlin_mobile_1x/private/image/Fernsehturm_iStock_c_bluejayphoto_DL_PPT_0.jpg?h=cbc5e7bf\u0026itok=MfN7PE-c",
    created_at: "2020-04-01T13:16:50.524Z",
    updated_at: "2020-04-15T02:14:24.091Z",
  },
  {
    id: 6,
    title: "València",
    content:
      "La ciudad portuaria de Valencia se ubica en la costa sureste de España, donde el río Turia se une al mar Mediterráneo. Es famosa por la Ciudad de las Artes y las Ciencias, con estructuras futurísticas, como el planetario, el oceanario y un museo interactivo. Valencia también tiene varias playas, incluidas algunas dentro del cercano Parque de la Albufera.",
    lat: "39.4697500",
    long: "-0.3773900",
    image_url:
      "https://valenciasecreta.com/wp-content/uploads/2019/10/valencia-rutas-turisticas.jpg",
    created_at: "2020-04-15T02:17:38.010Z",
    updated_at: "2020-04-15T02:17:38.010Z",
  },
  {
    id: 1,
    title: "Madrid",
    content:
      "Madrid is the capital of Spain and the largest municipality in both the Community of Madrid and Spain as a whole.",
    lat: "35.6895000",
    long: "139.6917100",
    image_url: "https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg",
    created_at: "2020-04-01T13:16:50.512Z",
    updated_at: "2020-04-15T00:57:14.864Z",
  },
  {
    id: 2,
    title: "Barcelona",
    content:
      "Barcelona is the capital and largest city of Catalonia with a population of 1.6 million within city limits. ddsd",
    lat: "41.3852",
    long: "2.1734",
    image_url:
      "https://static.independent.co.uk/s3fs-public/styles/story_medium/public/thumbnails/image/2017/05/17/15/barcelona-skyline.jpg",
    created_at: "2020-04-01T13:16:50.519Z",
    updated_at: "2020-04-15T01:39:38.734Z",
  },
];
