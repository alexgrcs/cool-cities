import listResults from "./listResults";

export default {
  get: jest.fn().mockResolvedValue({ data: listResults }),
};
