export default {
  id: 5,
  title: "Tokio",
  content:
    "El Área del Gran Tokio se denomina a la amplia área metropolitana de Tokio, Japón. Es una de las áreas metropolitanas con mayor población en el mundo. En japonés, Área del Gran Tokio se puede referir tanto a Área de Tokio (東京圏 Tōkyō-ken?), como a Capital (首都圏 Shuto-ken?) o Sur de Kanto (南関東 Minami-Kantō?). Esta área es la primera en el mundo en el uso de tránsito rápido, con 22 millones de pasajeros que utilizan diariamente las 136 líneas de tren.",
  lat: "35.6894989",
  long: "139.6917100",
  image_url:
    "https://heymondo.es/blog/wp-content/uploads/2018/08/tokio-en-3-d%C3%ADas.jpg",
  created_at: "2020-04-15T02:03:22.618Z",
  updated_at: "2020-04-15T02:03:54.209Z",
};
