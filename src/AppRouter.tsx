import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home";

function AppRouter() {
  return (
    <Router>
      <Switch>
        <Route exact path="/cities/:postSingle?/:edit?">
          <Home />
        </Route>
        <Route exact path="/new">
          <Home />
        </Route>
        <Redirect to="/cities" />
      </Switch>
    </Router>
  );
}

export default AppRouter;
