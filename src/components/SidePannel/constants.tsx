import { Post } from "../common/types/Post";

export const EMPTY_POST: Post = {
  title: "",
  content: "",
  lat: "",
  long: "",
  image_url: "",
};
