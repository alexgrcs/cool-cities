import React from "react";
import {
  render,
  cleanup,
  screen,
  waitForElement,
} from "@testing-library/react";
import SidePannel from "./";

afterEach(cleanup);

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

it("renders preview", async () => {
  const mockPostId = "3";
  const mockIsEditable = false;

  render(<SidePannel postId={mockPostId} isEditable={mockIsEditable} />);

  await waitForElement(() => screen.getByRole("heading"));

  expect(screen.getByRole("heading")).toBeInTheDocument();
});

it("renders form", async () => {
  const mockPostId = "3";
  const mockIsEditable = true;

  render(<SidePannel postId={mockPostId} isEditable={mockIsEditable} />);

  await waitForElement(() => screen.getByRole("form"));

  expect(screen.getByRole("form")).toBeInTheDocument();
});
