import styled from "styled-components";

export const OverlayWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.25);
`;

export const HideButtonWrapper = styled.div`
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 15%;
  height: 100%;
  cursor: pointer;

  @media (max-width: 960px) {
    width: 10%;
  }
`;

export const SidePannelWrapper = styled.div`
  position: relative;
  width: 85%;
  height: 100%;
  overflow: auto;
  background-color: #ffffff;

  @media (max-width: 960px) {
    width: 90%;
  }
`;
