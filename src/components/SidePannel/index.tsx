import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Post } from "../common/types/Post";
import {
  getPostById,
  updatePostById,
  createPost,
  deletePostById,
} from "../../api";
import Loader from "../Loader";
import Preview from "./components/Preview";
import EditPost from "./components/EditPost";
import { OverlayWrapper, HideButtonWrapper, SidePannelWrapper } from "./styles";
import { EMPTY_POST } from "./constants";

const NEW_POST_ID = "new";

type Props = {
  postId: string;
  isEditable: boolean;
};

function SidePannel({ postId, isEditable }: Props) {
  const history = useHistory();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [post, setPost] = useState<Post | null>(null);

  async function fetchPostById(id: string) {
    if (id === NEW_POST_ID) {
      return;
    }

    setIsLoading(true);

    const post = await getPostById(id);

    setIsLoading(false);
    setPost(post);
  }

  useEffect(() => {
    fetchPostById(postId);
  }, [postId, isEditable]);

  function goToPath(pathname: string) {
    history.replace({ pathname });
  }

  function handleHidePannel() {
    goToPath("/cities");
  }

  function handlePreviewPost() {
    goToPath(`/cities/${postId}`);
  }

  function handleEditPost() {
    goToPath(`/cities/${postId}/edit`);
  }

  async function handleSubmitEdit(post: Post) {
    setIsLoading(true);

    await updatePostById(postId, post);

    setIsLoading(false);
    goToPath(`/cities/${postId}`);
  }

  async function handleSubmitNew(post: Post) {
    setIsLoading(true);

    await createPost(post);

    setIsLoading(false);
    goToPath(`/cities`);
  }

  async function handleSubmitDelete() {
    setIsLoading(true);

    await deletePostById(postId);

    setIsLoading(false);
    goToPath(`/cities`);
  }

  return (
    <OverlayWrapper>
      <HideButtonWrapper onClick={handleHidePannel} />
      <SidePannelWrapper>
        {isLoading && <Loader />}
        {!isLoading && postId === NEW_POST_ID && (
          <EditPost
            post={EMPTY_POST}
            onCancel={handleHidePannel}
            onSubmit={handleSubmitNew}
          />
        )}
        {!isLoading && post && !isEditable && (
          <Preview
            post={post}
            onEdit={handleEditPost}
            onCancel={handleHidePannel}
            onDelete={handleSubmitDelete}
            data-testid="preview"
          />
        )}
        {!isLoading && post && isEditable && (
          <EditPost
            post={post}
            onCancel={handlePreviewPost}
            onSubmit={handleSubmitEdit}
          />
        )}
      </SidePannelWrapper>
    </OverlayWrapper>
  );
}

export default SidePannel;
