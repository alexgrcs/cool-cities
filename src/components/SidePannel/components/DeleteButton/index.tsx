import React, { useState } from "react";
import { ButtonsGroupWrapper } from "./styles";
import {
  ButtonDangerWrapper,
  ButtonPrimaryWrapper,
} from "../../../common/styles";

type Props = {
  onDelete: Function;
};

function DeleteButton({ onDelete }: Props) {
  const [hasBeenClicked, setHasBeenClicked] = useState(false);

  if (!hasBeenClicked) {
    return (
      <ButtonDangerWrapper
        type="button"
        onClick={() => setHasBeenClicked(true)}
        data-testid="button-trigger"
      >
        Delete
      </ButtonDangerWrapper>
    );
  }

  return (
    <ButtonsGroupWrapper data-testid="buttons-wrapper">
      <span>Are you sure?</span>
      <ButtonPrimaryWrapper type="button" onClick={() => onDelete()}>
        Yes
      </ButtonPrimaryWrapper>
    </ButtonsGroupWrapper>
  );
}

export default DeleteButton;
