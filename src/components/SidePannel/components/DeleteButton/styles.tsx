import styled from "styled-components";

export const ButtonsGroupWrapper = styled.div`
  display: flex;
  align-items: center;

  & > span {
    display: inline-block;
    margin-right: 1em;
  }

  @media (max-width: 960px) {
    flex-direction: column-reverse;
    justify-content: flex-end;

    & > span {
      margin-top: 1em;
      margin-right: 0;
    }
  }
`;
