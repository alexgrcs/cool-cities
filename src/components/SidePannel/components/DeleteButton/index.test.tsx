import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import DeleteButton from "./";

afterEach(cleanup);

const mockOnDelete = () => {};

it("renders properly before click", () => {
  const { asFragment } = render(<DeleteButton onDelete={mockOnDelete} />);
  expect(asFragment()).toMatchSnapshot();
});

it("renders properly after click", () => {
  render(<DeleteButton onDelete={mockOnDelete} />);

  fireEvent.click(screen.getByTestId("button-trigger"));

  expect(screen.getByTestId("buttons-wrapper")).toBeInTheDocument();
});
