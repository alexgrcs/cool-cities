import React, { useState } from "react";
import { Post } from "../../../common/types/Post";
import {
  InputWrapper,
  TextareaWrapper,
  ButtonWrapper,
  ButtonPrimaryWrapper,
  LabelWrapper,
} from "../../../common/styles";
import { FormWrapper } from "./styles";

type Props = {
  post: Post;
  onSubmit: Function;
  onCancel: Function;
};

function EditForm({ post, onSubmit, onCancel }: Props) {
  const { created_at, updated_at, ...formPost } = post;
  const [formData, setFormData] = useState(formPost);

  const handleInputChange = (
    event:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    setFormData({ ...formData, [event.target.id]: event.target.value });
  };

  const isSaveAllowed = () => {
    const formValues = Object.values(formData);
    let isAllowed = true;

    for (let index = 0; index < formValues.length; index++) {
      if (!formValues[index]) {
        isAllowed = false;
      }
    }

    return isAllowed;
  };

  const handleOnSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!isSaveAllowed()) {
      return;
    }

    onSubmit(formData);
  };

  return (
    <FormWrapper onSubmit={handleOnSubmit} role="form">
      <LabelWrapper>City Name</LabelWrapper>
      <InputWrapper
        type="text"
        id="title"
        value={formData.title}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleInputChange(event)
        }
      />
      <LabelWrapper>Description</LabelWrapper>
      <TextareaWrapper
        id="content"
        value={formData.content}
        onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) =>
          handleInputChange(event)
        }
      />
      <LabelWrapper>Image URL</LabelWrapper>
      <InputWrapper
        type="text"
        id="image_url"
        value={formData.image_url}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleInputChange(event)
        }
      />
      <LabelWrapper>Map Latitude</LabelWrapper>
      <InputWrapper
        type="text"
        id="lat"
        value={formData.lat}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleInputChange(event)
        }
      />
      <LabelWrapper>Map Longitude</LabelWrapper>
      <InputWrapper
        type="text"
        id="long"
        value={formData.long}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleInputChange(event)
        }
      />
      <ButtonPrimaryWrapper type="submit" disabled={!isSaveAllowed()}>
        Save
      </ButtonPrimaryWrapper>
      <ButtonWrapper type="button" onClick={() => onCancel()}>
        Cancel
      </ButtonWrapper>
    </FormWrapper>
  );
}

export default EditForm;
