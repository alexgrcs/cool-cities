import React from "react";
import { render, cleanup } from "@testing-library/react";
import EditForm from "./";
import singleResults from "../../../../__mocks__/singleResults";

afterEach(cleanup);

const mockClick = () => {};
const mockPost = singleResults;

it("renders", () => {
  const { asFragment } = render(
    <EditForm post={mockPost} onSubmit={mockClick} onCancel={mockClick} />
  );
  expect(asFragment()).toMatchSnapshot();
});
