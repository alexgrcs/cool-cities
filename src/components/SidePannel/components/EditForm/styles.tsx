import styled from "styled-components";

export const FormWrapper = styled.form`
  width: 100%;
  max-width: 600px;
`;
