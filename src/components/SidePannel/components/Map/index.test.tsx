import React from "react";
import { render, cleanup } from "@testing-library/react";
import Map from "./";

afterEach(cleanup);

const mockLat = 35.6894989;
const mockLong = 139.6917114;

it("renders", () => {
  const { asFragment } = render(<Map lat={mockLat} long={mockLong} />);
  expect(asFragment()).toMatchSnapshot();
});
