import styled from "styled-components";

export const MapWrapper = styled.div`
  display: flex;
  width: 50%;
  height: 100%;
  background-color: #eeeeee;

  @media (max-width: 960px) {
    width: 100%;
    height: 300px;
    padding: 0 40px 40px;
    background-color: transparent;
  }

  & > div {
    width: 100% !important;
    height: 100% !important;
  }
`;
