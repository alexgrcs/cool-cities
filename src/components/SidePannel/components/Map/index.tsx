import React, { useState, useEffect } from "react";
import ReactMapGL from "react-map-gl";
import { MapWrapper } from "./styles";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoiYWxleGdyY3MiLCJhIjoiY2s5MDFtbW92MHR0cTNlczIzNzRkajQ1byJ9.fRyxHezzFJW1qwaRPOOgwA";

const DEFAULT_SIZE = 350;
const DEFAULT_ZOOM = 11;

type Props = {
  lat: number;
  long: number;
};

interface Viewport {
  width: number;
  height: number;
  latitude: number;
  longitude: number;
  zoom: number;
}

function MapCanvas({ lat, long }: Props) {
  const [viewport, setViewport] = useState<Viewport | null>(null);

  useEffect(() => {
    const initViewport = {
      width: DEFAULT_SIZE,
      height: DEFAULT_SIZE,
      latitude: lat,
      longitude: long,
      zoom: DEFAULT_ZOOM,
    };

    setViewport(initViewport);
  }, [lat, long]);

  return (
    <MapWrapper>
      {viewport && (
        <ReactMapGL
          {...viewport}
          onViewportChange={(viewport) => setViewport(viewport)}
          mapboxApiAccessToken={MAPBOX_TOKEN}
        />
      )}
    </MapWrapper>
  );
}

export default MapCanvas;
