import React from "react";
import { render, cleanup } from "@testing-library/react";
import EditPost from "./";
import singleResults from "../../../../__mocks__/singleResults";

afterEach(cleanup);

const mockClick = () => {};
const mockPost = singleResults;

it("renders", () => {
  const { asFragment } = render(
    <EditPost post={mockPost} onSubmit={mockClick} onCancel={mockClick} />
  );
  expect(asFragment()).toMatchSnapshot();
});
