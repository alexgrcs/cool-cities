import React from "react";
import { Post } from "../../../common/types/Post";
import EditForm from "../EditForm";
import { MainWrapper, FormWrapper } from "./styles";

type Props = {
  post: Post;
  onSubmit: Function;
  onCancel: Function;
};

function EditPost({ post, onSubmit, onCancel }: Props) {
  return (
    <MainWrapper>
      <FormWrapper>
        <EditForm post={post} onSubmit={onSubmit} onCancel={onCancel} />
      </FormWrapper>
    </MainWrapper>
  );
}

export default EditPost;
