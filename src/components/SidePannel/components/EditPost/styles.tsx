import styled from "styled-components";

export const MainWrapper = styled.div`
  display: flex;
  width: 100%;
`;

export const FormWrapper = styled.div`
  width: 100%;
  padding: 40px 40px 60px;
`;
