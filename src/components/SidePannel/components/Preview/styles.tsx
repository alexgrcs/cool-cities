import styled from "styled-components";

export const MainWrapper = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  overflow: hidden;

  @media (max-width: 960px) {
    flex-direction: column;
    height: auto;
  }
`;

export const InfoWrapper = styled.div`
  width: 50%;
  flex-basis: 50%;
  padding: 20px 40px 60px;
  overflow: auto;

  p {
    font-size: 1rem;
    margin-bottom: 2em;
  }

  @media (max-width: 960px) {
    width: 100%;
    padding: 20px 40px 40px;
  }
`;

export const TitleWrapper = styled.h1`
  display: block;
  font-size: 2rem;
  font-weight: bold;
  line-height: 1.15;
  margin-bottom: 0.85em;
`;

export const ButtonGroupWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2em;
`;
