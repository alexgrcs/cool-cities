export function areLatAndLongValid(lat, long) {
  if (!lat || !long) {
    return false;
  }

  if (isNaN(lat) || isNaN(long)) {
    return false;
  }

  return true;
}
