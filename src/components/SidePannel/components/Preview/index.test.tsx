import React from "react";
import { render, cleanup } from "@testing-library/react";
import Preview from "./";
import singleResults from "../../../../__mocks__/singleResults";

afterEach(cleanup);

const mockClick = () => {};
const mockPost = singleResults;

it("renders", () => {
  const { asFragment } = render(
    <Preview
      post={mockPost}
      onEdit={mockClick}
      onCancel={mockClick}
      onDelete={mockClick}
    />
  );
  expect(asFragment()).toMatchSnapshot();
});
