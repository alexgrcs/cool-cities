import React from "react";
import { Post } from "../../../common/types/Post";
import { areLatAndLongValid } from "./utils";
import MapCanvas from "../Map";
import DeleteButton from "../DeleteButton";
import {
  MainWrapper,
  InfoWrapper,
  TitleWrapper,
  ButtonGroupWrapper,
} from "./styles";
import { ButtonWrapper, ButtonPrimaryWrapper } from "../../../common/styles";

type Props = {
  post: Post;
  onEdit: Function;
  onCancel: Function;
  onDelete: Function;
};

function Preview({ post, onEdit, onCancel, onDelete }: Props) {
  return (
    <MainWrapper>
      <InfoWrapper>
        <ButtonGroupWrapper>
          <div>
            <ButtonPrimaryWrapper type="submit" onClick={() => onEdit()}>
              Edit
            </ButtonPrimaryWrapper>
            <ButtonWrapper type="button" onClick={() => onCancel()}>
              Close
            </ButtonWrapper>
          </div>
          <DeleteButton onDelete={() => onDelete()} />
        </ButtonGroupWrapper>
        <TitleWrapper role="heading">{post.title}</TitleWrapper>
        <p>{post.content}</p>
        <img src={post.image_url} alt={post.title} />
      </InfoWrapper>
      {areLatAndLongValid(post.lat, post.long) && (
        <MapCanvas lat={Number(post.lat)} long={Number(post.long)} />
      )}
    </MainWrapper>
  );
}

export default Preview;
