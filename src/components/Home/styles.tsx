import styled from "styled-components";

interface MainWrapperProps {
  readonly noScroll: boolean;
}

export const MainWrapper = styled.div`
  overflow: ${(props: MainWrapperProps) =>
    props.noScroll ? "hidden" : "auto"};
  height: ${(props) => (props.noScroll ? "100vh" : "auto")};
`;

export const ContentWrapper = styled.div`
  display: block;
  width: 100%;
  max-width: 1168px;
  padding: 0 20px;
  margin: 40px auto 100px;

  & > ul {
    display: flex;
    flex-wrap: wrap;
    margin: 0;
    padding: 0;
    list-style: none;

    @media (max-width: 960px) {
      justify-content: center;
    }
  }
`;
