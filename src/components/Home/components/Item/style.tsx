import styled from "styled-components";

export const ItemWrapper = styled.li`
  width: 50%;
  min-height: 200px;
  padding: 0 0.515em 1.5em;

  @media (max-width: 960px) {
    width: 100%;
  }
`;

export const CardWrapper = styled.a`
  display: flex;
  height: 249px;
  max-width: 548px;
  margin: auto;
  background-color: #ffffff;
  border: 1px solid #dcdcdc;
  border-radius: 4px;
  box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1);
  overflow: hidden;
  cursor: pointer;

  @media (max-width: 540px) {
    flex-direction: column;
    height: auto;
  }
`;

export const InfoWrapper = styled.div`
  flex-basis: calc(100% - 200px);
  padding: 20px 20px 30px 30px;
  overflow: hidden;

  h2 {
    margin-bottom: 0.5em;
  }

  p {
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 6;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }
`;

export const ImageWrapper = styled.figure`
  flex-basis: 200px;
  position: relative;

  & > img {
    display: block;
    object-fit: cover;
    width: 100%;
    height: 100%;
    position: absolute;
  }
`;
