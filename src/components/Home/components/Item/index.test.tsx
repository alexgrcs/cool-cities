import React from "react";
import { render, cleanup } from "@testing-library/react";
import Item from "./";
import singleResults from "../../../../__mocks__/singleResults";

afterEach(cleanup);

it("renders", () => {
  const mockPost = singleResults;
  const mockOnClick = () => {};

  const { asFragment } = render(<Item post={mockPost} onClick={mockOnClick} />);
  expect(asFragment()).toMatchSnapshot();
});
