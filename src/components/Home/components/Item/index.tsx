import React from "react";

import { Post } from "../../../common/types/Post";

import { ItemWrapper, InfoWrapper, ImageWrapper, CardWrapper } from "./style";

type Props = {
  post: Post;
  onClick: Function;
};

function Item({ post, onClick }: Props) {
  return (
    <ItemWrapper role="article">
      <CardWrapper onClick={() => onClick()}>
        <InfoWrapper>
          <h2>{post.title}</h2>
          <p>{post.content}</p>
        </InfoWrapper>
        <ImageWrapper>
          <img src={post.image_url} alt={post.title} />
        </ImageWrapper>
      </CardWrapper>
    </ItemWrapper>
  );
}

export default Item;
