import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { ApiPost } from "../common/types/Post";
import { getPosts } from "../../api";
import Header from "../Header";
import SidePannel from "../SidePannel";
import Item from "./components/Item";
import { MainWrapper, ContentWrapper } from "./styles";
import Loader from "../Loader";

const EDIT_PARAM = "edit";

type Params = {
  postSingle?: string;
  edit?: string;
};

function Home() {
  const history = useHistory();
  const [posts, setPosts] = useState([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  let params = useParams();

  let { postSingle, edit }: Params = params;
  const isSidePannelOpen = Boolean(postSingle);
  const isEditable = edit === EDIT_PARAM;

  async function fetchPosts() {
    setIsLoading(true);

    const posts = await getPosts();

    setIsLoading(false);
    setPosts(posts);
  }

  useEffect(() => {
    fetchPosts();
  }, []);

  useEffect(() => {
    async function onRouteUpdate() {
      if (isEditable || postSingle) {
        return;
      }

      fetchPosts();
    }

    onRouteUpdate();
  }, [isEditable, postSingle]);

  function handleItemOnClick(post: ApiPost) {
    return () => {
      history.replace({ pathname: `/cities/${post.id}` });
    };
  }

  function handleOnAddClick() {
    history.replace({ pathname: "/cities/new" });
  }

  function getSortedPosts(posts: Array<ApiPost>) {
    return posts.sort((a: ApiPost, b: ApiPost) => {
      if (a.id > b.id) {
        return -1;
      }
      if (a.id < b.id) {
        return 1;
      }
      return 0;
    });
  }

  return (
    <MainWrapper noScroll={isSidePannelOpen}>
      <Header onAddClick={() => handleOnAddClick()} />
      <ContentWrapper>
        {isLoading ? (
          <Loader />
        ) : (
          <ul>
            {getSortedPosts(posts).map((post: ApiPost) => (
              <Item
                key={post.id}
                post={post}
                onClick={handleItemOnClick(post)}
              />
            ))}
          </ul>
        )}
      </ContentWrapper>
      {isSidePannelOpen && postSingle && (
        <SidePannel postId={postSingle} isEditable={isEditable} />
      )}
    </MainWrapper>
  );
}

export default Home;
