import React from "react";
import {
  render,
  cleanup,
  screen,
  waitForElement,
} from "@testing-library/react";
import Home from "./";

afterEach(cleanup);

jest.mock("axios");

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn(),
    replace: jest.fn(),
  }),
  useParams: () => ({
    postSingle: "",
    edit: "",
  }),
}));

it("renders posts list", async () => {
  render(<Home />);

  await waitForElement(() => screen.getByRole("list"));

  expect(screen.getByRole("list")).toBeInTheDocument();
});
