export interface Post {
  id?: number;
  title: string;
  content: string;
  lat: string;
  long: string;
  image_url: string;
  created_at?: string;
  updated_at?: string;
}

export interface ApiPost extends Post {
  id: number;
  created_at: string;
  updated_at: string;
}
