import styled, { css } from "styled-components";

const inputBase = css`
  display: block;
  width: 100%;
  height: calc(1.5em + 1rem + 2px);
  padding: 0.5rem 1rem;
  font-size: 1rem;
  line-height: 1.5;
  font-weight: 400;
  color: #707070;
  background-color: #ffffff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.3rem;
  margin-bottom: 1.25rem;
`;

export const InputWrapper = styled.input`
  ${inputBase}
`;

export const TextareaWrapper = styled.textarea`
  ${inputBase}
  resize: vertical;
  min-height: 150px;
  max-height: 400px;
`;

export const ButtonWrapper = styled.button`
  display: inline-block;
  font-size: 1.125rem;
  line-height: 1.5;
  font-weight: 600;
  color: #ffffff;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  user-select: none;
  background-color: #dedede;
  border: 1px solid transparent;
  padding: 0.5rem 1rem;
  border-radius: 0.3rem;
  margin-right: 0.5em;

  &:disabled {
    pointer-events: none;
    cursor: default;
    opacity: 0.65;
  }
`;

export const ButtonPrimaryWrapper = styled(ButtonWrapper)`
  background-color: #ff41b4;
`;

export const ButtonDangerWrapper = styled(ButtonWrapper)`
  background-color: #ff725c;
  margin-right: 0;
`;

export const LabelWrapper = styled.label`
  display: inline-block;
  margin-bottom: 0.5rem;
`;
