import React from "react";
import { render, cleanup } from "@testing-library/react";
import Header from "./";

afterEach(cleanup);

it("renders", () => {
  const mockOnAddClick = () => {};

  const { asFragment } = render(<Header onAddClick={mockOnAddClick} />);
  expect(asFragment()).toMatchSnapshot();
});
