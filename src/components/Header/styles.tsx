import styled from "styled-components";
import { ButtonPrimaryWrapper } from "../common/styles";

export const HeaderWrapper = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 77px;
  width: 100%;
  padding: 0 10px;
  color: #ffffff;
  background-color: #373737;
`;

export const LogoWrapper = styled.h1`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
  max-width: 166px;
  max-height: 54px;
  font-family: "Sriracha", cursive;
  font-size: 30px;
  text-align: center;
  margin: 0;
  color: #ffffff;
  background-color: transparent;
`;

export const AddButtonWrapper = styled(ButtonPrimaryWrapper)`
  text-transform: uppercase;
`;
