import React from "react";
import { HeaderWrapper, LogoWrapper, AddButtonWrapper } from "./styles";

type Props = {
  onAddClick: Function;
};

function Header({ onAddClick }: Props) {
  return (
    <HeaderWrapper>
      <LogoWrapper>
        <span>Cool Cities</span>
      </LogoWrapper>
      <AddButtonWrapper onClick={() => onAddClick()}>
        Add New City
      </AddButtonWrapper>
    </HeaderWrapper>
  );
}

export default Header;
