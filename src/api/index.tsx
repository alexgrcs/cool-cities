import axios, { AxiosResponse } from "axios";
import { Post } from "../components/common/types/Post";

const API_BASE_URL =
  "https://wf-challenge-d6haqugtoo.herokuapp.com/api/v1/posts";

export function getPosts() {
  return axios.get(`${API_BASE_URL}`).then((response: AxiosResponse) => {
    return response.data;
  });
}

export function getPostById(id: string) {
  return axios.get(`${API_BASE_URL}/${id}`).then((response: AxiosResponse) => {
    return response.data;
  });
}

export function updatePostById(id: string, body: Post) {
  return axios
    .put(`${API_BASE_URL}/${id}`, body)
    .then((response: AxiosResponse) => {
      return response.data;
    });
}

export function createPost(body: Post) {
  return axios.post(`${API_BASE_URL}`, body).then((response: AxiosResponse) => {
    return response.data;
  });
}

export function deletePostById(id: string) {
  return axios
    .delete(`${API_BASE_URL}/${id}`)
    .then((response: AxiosResponse) => {
      return response.data;
    });
}
